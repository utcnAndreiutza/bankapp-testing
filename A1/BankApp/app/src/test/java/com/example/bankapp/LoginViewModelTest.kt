package com.example.bankapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.bankapp.business_layer.classes.LoginBusinessClass
import com.example.bankapp.database_layer.AdminDao
import com.example.bankapp.database_layer.EmployeeDao
import com.example.bankapp.domain_model_layer.Admin
import com.example.bankapp.domain_model_layer.DisplayableUser
import com.example.bankapp.domain_model_layer.Employee
import com.nhaarman.mockito_kotlin.*
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito


inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

class LoginViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: LoginBusinessClass
    private val userObserver: Observer<DisplayableUser> = mock()
    private val adminDao: AdminDao = mock()
    private val employeeDao: EmployeeDao = mock()

    companion object {
        private const val EMPLOYEE_EMAIL = "user@mail.com"
        private const val UNKNOWN_EMAIL_USER = "unknown@mail.com"
        private const val UNKNOWN_EMAIL_PASS = "passUnknown"
        private const val EMAIL_ADMIN = "admin@mail.com"
        private const val WRONG_EMAIL = "wrongadmin@mail.com"
        private const val INVALID_PASS = "adm"
        private const val EMPLOYEE_PASS = "user"
        private const val PASS_ADMIN = "admin"
        private const val NAME = "Andreea"
    }

    @Before
    fun before() {
        viewModel = LoginBusinessClass(adminDao, employeeDao)
        viewModel.userType.observeForever(userObserver)
    }

    @After
    fun clean() {
        viewModel.userType.removeObserver(userObserver)
    }

    @Test
    fun loginAdmin() {
        whenever(adminDao.login(EMAIL_ADMIN, PASS_ADMIN)).thenReturn(
            Admin(
                0,
                NAME,
                "2136185165151",
                "Adresa",
                "0759594593",
                EMAIL_ADMIN,
                PASS_ADMIN
            )
        )
        whenever(employeeDao.login(EMAIL_ADMIN, PASS_ADMIN)).thenReturn(null)

        viewModel.login(EMAIL_ADMIN, PASS_ADMIN)
        val captor = ArgumentCaptor.forClass(DisplayableUser::class.java)
        verify(userObserver).onChanged(captor.capture())
        val loginResult = captor.value
        assertNotNull(loginResult)
        assertEquals(EMAIL_ADMIN, loginResult.email)
    }

    @Test
    fun loginEmployee() {
        whenever(employeeDao.login(EMPLOYEE_EMAIL, EMPLOYEE_PASS)).thenReturn(
            Employee(
                0,
                NAME,
                "2136185165151",
                "Adresa",
                "0759594593",
                EMPLOYEE_EMAIL,
                EMPLOYEE_PASS
            )
        )
        whenever(adminDao.login(EMPLOYEE_EMAIL, EMPLOYEE_PASS)).thenReturn(null)

        viewModel.login(EMPLOYEE_EMAIL, EMPLOYEE_PASS)
        val captor = ArgumentCaptor.forClass(DisplayableUser::class.java)
        captor.run {
            verify(userObserver, times(1)).onChanged(capture())
            assertNotNull(value)
            assertEquals(EMPLOYEE_EMAIL, value.email)
        }
    }

    @Test
    fun loginUserNotFound() {
        whenever(employeeDao.login(WRONG_EMAIL, EMPLOYEE_PASS)).thenReturn(null)
        whenever(adminDao.login(WRONG_EMAIL, EMPLOYEE_PASS)).thenReturn(null)

        viewModel.login(WRONG_EMAIL, EMPLOYEE_PASS)
        val captor = ArgumentCaptor.forClass(DisplayableUser::class.java)
        captor.run {
            verify(userObserver, times(1)).onChanged(capture())
            assertEquals(value, null)
        }
    }

    @Test
    fun loginWrongPassword() {
        whenever(employeeDao.login(EMPLOYEE_EMAIL, INVALID_PASS)).thenReturn(null)
        whenever(adminDao.login(EMPLOYEE_EMAIL, INVALID_PASS)).thenReturn(null)

        viewModel.login(WRONG_EMAIL, EMPLOYEE_PASS)
        val captor = ArgumentCaptor.forClass(DisplayableUser::class.java)
        captor.run {
            verify(userObserver, times(1)).onChanged(capture())
            assertEquals(value, null)
        }
    }

    @Test
    fun loginInvalidEmail() {
        viewModel.login(UNKNOWN_EMAIL_USER, UNKNOWN_EMAIL_PASS)
        val captor = ArgumentCaptor.forClass(DisplayableUser::class.java)
        captor.run {
            verify(userObserver, times(1)).onChanged(capture())
            assertEquals(value, null)
        }
    }

}