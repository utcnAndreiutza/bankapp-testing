package com.example.bankapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.bankapp.business_layer.classes.AdminBusinessClass
import com.example.bankapp.database_layer.AdminDao
import com.example.bankapp.database_layer.EmployeeDao
import com.example.bankapp.domain_model_layer.Employee
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import junit.framework.Assert.*
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor


class AdminViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: AdminBusinessClass
    private val employeeDao: EmployeeDao = mock()
    private val adminDao: AdminDao = mock()
    private val employeeObserver: Observer<Employee> = mock()
    private val employeeListObserver: Observer<List<Employee>> = mock()

    private var employeeList: ArrayList <Employee> = ArrayList()
    companion object {
        private const val FIRST_USER_NAME = "Lucica Hill"
        private const val SECOND_USER_NAME = "Marius Brass"
        private const val THIRD_USER_NAME = "Andreea Lirey"
    }

    @Before
    fun before() {
        initEmployeeList()
        viewModel = AdminBusinessClass(adminDao, employeeDao, null)
        viewModel.employee.observeForever(employeeObserver)
        viewModel.employeesList.observeForever(employeeListObserver)
    }

    @After
    fun clean() {
        viewModel.employee.removeObserver(employeeObserver)
        viewModel.employeesList.removeObserver(employeeListObserver)
    }

    @Test
    fun getAllEmployees(){
        whenever(employeeDao.getAllEmployees()).thenReturn(employeeList)

        viewModel.getAllEmployees()
        val captor = argumentCaptor<List<Employee>>()
        verify(employeeListObserver).onChanged(captor.capture())
        val givenEmployeeList = captor.allValues[0]
        assertNotNull(givenEmployeeList)
        assertEquals(3,givenEmployeeList.size)
        assertEquals(givenEmployeeList[0].name , FIRST_USER_NAME)
        assertEquals(givenEmployeeList[1].name , SECOND_USER_NAME)
        assertEquals(givenEmployeeList[2].name , THIRD_USER_NAME)
        assertEquals(givenEmployeeList, employeeList)
    }

    private fun initEmployeeList(){
        employeeList =  ArrayList()
        employeeList.add(Employee(name = FIRST_USER_NAME, email = "employee1@mail.com", password = "pswd1", cnp = "2720101416241", adress="Str. Mihai Viteazu nr20", phoneNumber = "0749366921"))
        employeeList.add(Employee(name = SECOND_USER_NAME,email = "employee2@mail.com", password = "pswd2", cnp = "1720101416641",adress="Str. Garoafelor nr21", phoneNumber = "0740066921"))
        employeeList.add(Employee(name = THIRD_USER_NAME,email = "employee3@mail.com", password = "pswd3",cnp = "2720101576241",adress="Str. Sighisoarei nr22", phoneNumber = "0749300921"))
    }

    @Test
    fun getEmployeeWithId(){
        val testId =1
        val size =employeeList.size-1
        val randomEmployee = employeeList[(0..size).random()]
        whenever(employeeDao.getEmployeeById(testId)).thenReturn(randomEmployee)

        viewModel.getEmployeeWithId(testId)

        val captor = ArgumentCaptor.forClass(Employee::class.java)
        verify(employeeObserver).onChanged(captor.capture())
        assertNotNull(captor.value)
        assertEquals(captor.value,randomEmployee)
    }

    @Test
    fun updateEmployee(){
        val testId =1
        val updatedEmployee = employeeList[testId]
        updatedEmployee.adress = "Str. Marcus Lewis, nr 223"
        updatedEmployee.email = "mariMarcus@gmail.com"
        updatedEmployee.phoneNumber = "0795044869"
        whenever(employeeDao.updateEmployee(updatedEmployee.id,updatedEmployee.name, updatedEmployee.cnp, updatedEmployee.adress, updatedEmployee.phoneNumber, updatedEmployee.email)).then {
            employeeList[testId].name = updatedEmployee.name
            employeeList[testId].cnp = updatedEmployee.cnp
            employeeList[testId].adress = updatedEmployee.adress
            employeeList[testId].phoneNumber = updatedEmployee.phoneNumber
            employeeList[testId].email = updatedEmployee.email

            assertEquals(employeeList[testId],updatedEmployee)
        }

        viewModel.updateEmployee(updatedEmployee)

    }

    @Test
    fun insertEmployee(){
        val testId =4
        val newEmployee = Employee(name = "Laura Tiner", email = "lauraTiner@mail.com", password = "pswd1", cnp = "2720101416241", adress="Str. Mentiseni nr20", phoneNumber = "0749366921")
        whenever(employeeDao.insertEmployee(newEmployee)).then {
            employeeList.add(newEmployee)
            assertEquals(employeeList[testId],newEmployee)
        }

        viewModel.insertEmployee(newEmployee)

    }

    @Test
    fun deleteEmployeeWithId(){
        val testId =1
        whenever(employeeDao.deleteEmployeeWithId(testId)).then {
            val previousEmployeeList = employeeList.size
            employeeList.removeAt(testId)
            assertNotEquals(employeeList.size,previousEmployeeList)
            assertEquals(employeeList.size,previousEmployeeList-1)
        }

        viewModel.deleteEmployeeWithId(testId)
    }

    @Test
    fun isEmployeeDataValid(){
        val invalidEmailUser =  Employee(name = "Laura Tiner", email = "lauraTiner", password = "pswd1", cnp = "2720101416241", adress="Str. Mentiseni nr20", phoneNumber = "0749366921")
        val invalidCNPUser =  Employee(name = "Laura Tiner", email = "lauraTiner@mail.com", password = "pswd1", cnp = "41", adress="Str. Mentiseni nr20", phoneNumber = "0749366921")
        val invalidPhoneNumberUser =  Employee(name = "Laura Tiner", email = "lauraTiner@mail.com", password = "pswd1", cnp = "2720101416241", adress="Str. Mentiseni nr20", phoneNumber = "07")
        val invalidEmptyFieldsUser =  Employee(name = "", email = "lauraTiner@mail.com", password = "pswd1", cnp = "2720101416241", adress="", phoneNumber = "07")

        val isValidUser = viewModel.isEmployeeDataValid(employeeList[0])
        val isInValidEmailUser = viewModel.isEmployeeDataValid(invalidEmailUser)
        val isInvalidCNPUser = viewModel.isEmployeeDataValid(invalidCNPUser)
        val isInvalidPhoneNumberUser = viewModel.isEmployeeDataValid(invalidPhoneNumberUser)
        val isInvalidEmptyFieldsUser = viewModel.isEmployeeDataValid(invalidEmptyFieldsUser)

        assertTrue(isValidUser)
        assertFalse(isInValidEmailUser)
        assertFalse(isInvalidCNPUser)
        assertFalse(isInvalidPhoneNumberUser)
        assertFalse(isInvalidEmptyFieldsUser)
    }
}