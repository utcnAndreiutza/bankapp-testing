package com.example.bankapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.bankapp.business_layer.classes.EmployeeBusinessClass
import com.example.bankapp.database_layer.ClientDao
import com.example.bankapp.domain_model_layer.Client
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import junit.framework.Assert.*
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import java.util.*
import kotlin.collections.ArrayList


class TransactionsTestClass{
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: EmployeeBusinessClass
    private val clientDao: ClientDao = mock()
    private val clientObserver: Observer<Client> = mock()
    private val clientListObserver: Observer<List<Client>> = mock()

    private var clientList: ArrayList <Client> = ArrayList()
    companion object {
        private const val FIRST_USER_NAME = "Wagnaer Auftiz"
        private const val SECOND_USER_NAME = "Levi Bodgor"
        private const val THIRD_USER_NAME = "Livia Iris"
    }

    @Before
    fun before() {
        initClientList()
        viewModel = EmployeeBusinessClass(clientDao, null)
        viewModel.client.observeForever(clientObserver)
        viewModel.clientList.observeForever(clientListObserver)
    }

    @After
    fun clean() {
        viewModel.client.removeObserver(clientObserver)
        viewModel.clientList.removeObserver(clientListObserver)
    }

    private fun initClientList(){
        clientList =  ArrayList()
        clientList.add(Client(name = FIRST_USER_NAME, email = "client1@mail.com",cardId = "client1System0001",phoneNumber = "0846255793",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "Str. Sighisoarei nr120"))
        clientList.add(Client(name = SECOND_USER_NAME, email = "clien2@mail.com",cardId = "client2System0002",phoneNumber = "0846255733",cnp = "1032132145002",amount = 100.0,creationDate = Date(),adress = "Str. Sighisoarei nr220"))
        clientList.add(Client(name = THIRD_USER_NAME, email = "client3@mail.com",cardId = "client3System0003",phoneNumber = "0846275793",cnp = "1021145678043",amount = 50.0,creationDate = Date(),adress = "Str. Sighisoarei nr190"))
    }

    @Test
    fun isAmountValid(){
        val isValidAmount = viewModel.isAmountValid(300.0)
        val isInvalidAmount = viewModel.isAmountValid(-300.0)
        val isInvalidAmount2 = viewModel.isAmountValid(0.0)

        assertTrue(isValidAmount)
        assertFalse(isInvalidAmount)
        assertFalse(isInvalidAmount2)
    }

    @Test
    fun areAmountsValid(){
        val areValidAmounts = viewModel.areAmountsValid((1..1000).random().toString(),(1..1000).random().toString(),(1..1000).random().toString())
        val areInvalidAmounts = viewModel.areAmountsValid((-1000..0).random().toString(),(-1000..0).random().toString(),(-1000..0).random().toString())
        val areInvalidAmounts2 = viewModel.areAmountsValid((1..1000).random().toString(),(-1000..0).random().toString(),(-1000..0).random().toString())
        val areInvalidAmounts3 = viewModel.areAmountsValid((-1000..0).random().toString(),(1..1000).random().toString(),(-1000..0).random().toString())
        val areInvalidAmounts4 = viewModel.areAmountsValid((-1000..0).random().toString(),(-1000..0).random().toString(),(1..1000).random().toString())

        assertTrue(areValidAmounts)
        assertFalse(areInvalidAmounts)
        assertFalse(areInvalidAmounts2)
        assertFalse(areInvalidAmounts3)
        assertFalse(areInvalidAmounts4)
    }

    @Test
    fun generateCardId(){
        val generatedCardId = viewModel.generateCardId()
        val firstSectionOfString = generatedCardId.subSequence(0,2)
        assertEquals(firstSectionOfString,"ID")
        assertEquals(generatedCardId.length,7)
    }

    @Test
    fun transactionBetweenAccounts(){
        val sendMoneyAccountId =2
        val receiveMoneyAccountId =1
        val testAmount =24.2
        val previousSendMoneyAccountAmount = clientList[sendMoneyAccountId].amount
        val previousReceiveMoneyAccountAmount = clientList[receiveMoneyAccountId].amount

        whenever(clientDao.addMoney(sendMoneyAccountId, testAmount)).then {
            clientList[sendMoneyAccountId].amount += testAmount
            assertNotEquals(clientList[sendMoneyAccountId].amount,previousSendMoneyAccountAmount)
            assertEquals(clientList[sendMoneyAccountId].amount,previousSendMoneyAccountAmount + testAmount)
        }
        whenever(clientDao.addMoney(receiveMoneyAccountId, testAmount)).then {
            clientList[receiveMoneyAccountId].amount += testAmount
            assertNotEquals(clientList[receiveMoneyAccountId].amount,previousReceiveMoneyAccountAmount)
            assertEquals(clientList[receiveMoneyAccountId].amount,previousReceiveMoneyAccountAmount + testAmount)
        }

        viewModel.transactionBetweenAccounts(sendMoneyAccountId,receiveMoneyAccountId.toString(), testAmount )
    }

    @Test
    fun extractMoneyFromAccount(){
        val testId =2
        val testAmount =-10.99
        val previousAccountAmount = clientList[testId].amount

        whenever(clientDao.addMoney(testId, testAmount)).then {
            clientList[testId].amount += testAmount
            assertNotEquals(clientList[testId].amount,previousAccountAmount)
            assertEquals(clientList[testId].amount,previousAccountAmount + testAmount)
        }

        viewModel.extractMoneyFromAccount(testId,testAmount)
    }

    @Test
    fun addMoney(){
        val testId =1
        val testAmount =78.64
        val previousAccountAmount = clientList[testId].amount

        whenever(clientDao.addMoney(testId, testAmount)).then {
            clientList[testId].amount += testAmount
            assertNotEquals(clientList[testId].amount,previousAccountAmount)
            assertEquals(clientList[testId].amount,previousAccountAmount + testAmount)
        }

        viewModel.addMoney(testId,testAmount)
    }
}