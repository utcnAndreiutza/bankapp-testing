package com.example.bankapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.bankapp.business_layer.classes.EmployeeBusinessClass
import com.example.bankapp.database_layer.ClientDao
import com.example.bankapp.domain_model_layer.Client
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import junit.framework.Assert.*
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import java.util.*
import kotlin.collections.ArrayList


class EmployeeViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: EmployeeBusinessClass
    private val clientDao: ClientDao = mock()
    private val clientObserver: Observer<Client> = mock()
    private val clientListObserver: Observer<List<Client>> = mock()

    private var clientList: ArrayList <Client> = ArrayList()
    companion object {
        private const val FIRST_USER_NAME = "Wagnaer Auftiz"
        private const val SECOND_USER_NAME = "Levi Bodgor"
        private const val THIRD_USER_NAME = "Livia Iris"
    }

    @Before
    fun before() {
        initClientList()
        viewModel = EmployeeBusinessClass(clientDao, null)
        viewModel.client.observeForever(clientObserver)
        viewModel.clientList.observeForever(clientListObserver)
    }

    @After
    fun clean() {
        viewModel.client.removeObserver(clientObserver)
        viewModel.clientList.removeObserver(clientListObserver)
    }

    @Test
    fun getAllClients(){
        whenever(clientDao.getAllClients()).thenReturn(clientList)

        viewModel.getAllClients()
        val captor = argumentCaptor<List<Client>>()
        verify(clientListObserver).onChanged(captor.capture())
        val givenEmployeeList = captor.allValues[0]
        assertNotNull(givenEmployeeList)
        assertEquals(3,givenEmployeeList.size)
        assertEquals(givenEmployeeList[0].name , FIRST_USER_NAME)
        assertEquals(givenEmployeeList[1].name , SECOND_USER_NAME)
        assertEquals(givenEmployeeList[2].name , THIRD_USER_NAME)
        assertEquals(givenEmployeeList, clientList)
    }

    private fun initClientList(){
        clientList =  ArrayList()
        clientList.add(Client(name = FIRST_USER_NAME, email = "client1@mail.com",cardId = "client1System0001",phoneNumber = "0846255793",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "Str. Sighisoarei nr120"))
        clientList.add(Client(name = SECOND_USER_NAME, email = "clien2@mail.com",cardId = "client2System0002",phoneNumber = "0846255733",cnp = "1032132145002",amount = 100.0,creationDate = Date(),adress = "Str. Sighisoarei nr220"))
        clientList.add(Client(name = THIRD_USER_NAME, email = "client3@mail.com",cardId = "client3System0003",phoneNumber = "0846275793",cnp = "1021145678043",amount = 50.0,creationDate = Date(),adress = "Str. Sighisoarei nr190"))
    }

    @Test
    fun getClientWithId(){
        val testId =1
        val size =clientList.size-1
        val randomEmployee = clientList[(0..size).random()]
        whenever(clientDao.getClientById(testId)).thenReturn(randomEmployee)

        viewModel.getClientWithId(testId)

        val captor = ArgumentCaptor.forClass(Client::class.java)
        verify(clientObserver).onChanged(captor.capture())
        assertNotNull(captor.value)
        assertEquals(captor.value,randomEmployee)
    }

    @Test
    fun updateClient(){
        val testId =1
        val updatedClient = clientList[testId]
        updatedClient.id = 1
        updatedClient.adress = "Str. Marcus Lewis, nr 223"
        updatedClient.email = "mariMarcus@gmail.com"
        updatedClient.phoneNumber = "0795044869"
        whenever(clientDao.updateClient(updatedClient.id!!,updatedClient.name, updatedClient.cnp, updatedClient.adress, updatedClient.phoneNumber, updatedClient.email, updatedClient.creationDate)).then {
            clientList[testId].name = updatedClient.name
            clientList[testId].cnp = updatedClient.cnp
            clientList[testId].adress = updatedClient.adress
            clientList[testId].phoneNumber = updatedClient.phoneNumber
            clientList[testId].email = updatedClient.email
            clientList[testId].creationDate = updatedClient.creationDate

            assertEquals(clientList[testId],updatedClient)
        }
        viewModel.updateClient(updatedClient)
    }

    @Test
    fun insertClient(){
        val testId =4
        val newEmployee = Client(name = "Claudiu Pavel", email = "claudiuPavel@mail.com",cardId = "claudiuPavelSystem0001",phoneNumber = "0846255793",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "Str. Clujului nr120")
        whenever(clientDao.insertClient(newEmployee)).then {
            clientList.add(newEmployee)
            assertEquals(clientList[testId],newEmployee)
        }

        viewModel.insertClient(newEmployee)
    }

    @Test
    fun deleteClientWithId(){
        val testId =1
        whenever(clientDao.deleteClientWithId(testId)).then {
            val previousEmployeeList = clientList.size
            clientList.removeAt(testId)
            assertNotEquals(clientList.size,previousEmployeeList)
            assertEquals(clientList.size,previousEmployeeList-1)
        }

        viewModel.deleteClientWithId(testId)
    }

    @Test
    fun isClientDataValid(){
        val invalidEmailUser =  Client(name = "Claudiu Pavel", email = "claudiuPav",cardId = "claudiuPavelSystem0001",phoneNumber = "0846255793",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "Str. Clujului nr120")
        val invalidCNPUser =  Client(name = "Claudiu Pavel", email = "claudiuPavel@mail.com",cardId = "claudiuPavelSystem0001",phoneNumber = "0846255793",cnp = "204",amount = 2000.0,creationDate = Date(),adress = "Str. Clujului nr120")
        val invalidPhoneNumberUser =  Client(name = "Claudiu Pavel", email = "claudiuPavel@mail.com",cardId = "claudiuPavelSystem0001",phoneNumber = "08",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "Str. Clujului nr120")
        val invalidEmptyFieldsUser = Client(name = "", email = "claudiuPavel@mail.com",cardId = "claudiuPavelSystem0001",phoneNumber = "0846255793",cnp = "2043200434301",amount = 2000.0,creationDate = Date(),adress = "")


        val isValidUser = viewModel.isClientDataValid(clientList[0])
        val isInValidEmailUser = viewModel.isClientDataValid(invalidEmailUser)
        val isInvalidCNPUser = viewModel.isClientDataValid(invalidCNPUser)
        val isInvalidPhoneNumberUser = viewModel.isClientDataValid(invalidPhoneNumberUser)
        val isInvalidEmptyFieldsUser = viewModel.isClientDataValid(invalidEmptyFieldsUser)

        assertTrue(isValidUser)
        assertFalse(isInValidEmailUser)
        assertFalse(isInvalidCNPUser)
        assertFalse(isInvalidPhoneNumberUser)
        assertFalse(isInvalidEmptyFieldsUser)
    }
}