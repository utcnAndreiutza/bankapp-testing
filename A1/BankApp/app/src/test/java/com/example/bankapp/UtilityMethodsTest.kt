package com.example.bankapp


import com.example.bankapp.business_layer.getDateFormat
import com.example.bankapp.business_layer.getDateFromString
import com.example.bankapp.business_layer.isEmailValidUtils
import org.junit.Test
import org.junit.Assert.*


class UtilsUnitTest {

    @Test
    fun email_isValid() {
        val email = "andreea@mail.com"
        val invalidEmail = "andreeamail"
        val invalidEmail2 = "andreeamail.com"
        val invalidEmail3 = "andreeamail@com"
        val invalidEmail4 = "@.com"
        val invalidEmail5 = "abc@.com"
        assertTrue(isEmailValidUtils(email))
        assertFalse(isEmailValidUtils(invalidEmail))
        assertFalse(isEmailValidUtils(invalidEmail2))
        assertFalse(isEmailValidUtils(invalidEmail3))
        assertFalse(isEmailValidUtils(invalidEmail4))
        assertFalse(isEmailValidUtils(invalidEmail5))
    }
    @Test
    fun date_string_pattern_conversion_isValid(){
        val stringDate ="01:00, 31.03.2020"
        val date = getDateFromString(stringDate)
        val stringDateResult = getDateFormat(date)
        assertEquals(stringDate, stringDateResult)
    }

}
