package com.example.bankapp

import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.bankapp.presentation_layer.LoginActivity
import com.example.bankapp.presentation_layer.MainActivity
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginAuthenticationTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<LoginActivity>()
    lateinit var  decorView : View

    @Before
    fun setUp() {
        activityScenarioRule.scenario.onActivity {
            decorView = it.window.decorView
        }
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.bankapp", appContext.packageName)
    }

    @Test
    fun loginAdmin() {
        onView(withId(R.id.userEmail)).perform(replaceText("admin@mail.com"))
        onView(withId(R.id.userPassword)).perform(replaceText("admin"))
        onView(withId(R.id.loginButton)).perform(click())
        onView(withId(R.id.logOutButton)).perform(click())
        onView(withId(R.id.nameEditText)).perform(replaceText("admin@mail.com"))
    }

    @Test
    fun loginEmployee() {
        onView(withId(R.id.userEmail)).perform(replaceText("employee1@mail.com"))
        onView(withId(R.id.userPassword)).perform(replaceText("employee1"))
        onView(withId(R.id.loginButton)).perform(click())
        onView(withId(R.id.logOutButton)).perform(click())
    }

    @Test
    fun loginUserNotFound() {
        onView(withId(R.id.userEmail)).perform(replaceText("userNotFound@m.com"))
        onView(withId(R.id.userPassword)).perform(replaceText("userNotFound123"))
        onView(withId(R.id.loginButton)).perform(click())
        //check if invalid loginInvalidEmail message is displayed.
        onView(withText(R.string.user_not_found)).
        inRoot(withDecorView(not(decorView)))
            .check(matches(isDisplayed()))
    }
}