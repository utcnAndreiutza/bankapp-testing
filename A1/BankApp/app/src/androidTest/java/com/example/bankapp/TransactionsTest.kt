package com.example.bankapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.bankapp.presentation_layer.employee.ClientDetailsActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TransactionsTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<ClientDetailsActivity>()


    @Test
    fun transferMoneyTest() {
        onView(withId(R.id.transferMoneyButton)).perform(click())
        onView(allOf(withId(R.id.transferMoneyLayout), isDisplayed()))

        onView(withId(R.id.moneyEditText)).perform(replaceText("10"))
        onView(withId(R.id.cardIdEditText)).perform(replaceText("1"))
        onView(withId(R.id.doneButtonTransfer)).perform(click())

        onView(allOf(withId(R.id.transferMoneyLayout), not(isDisplayed())))
    }

    @Test
    fun cancelTransferMoneyTest() {
        onView(withId(R.id.transferMoneyButton)).perform(click())
        onView(allOf(withId(R.id.transferMoneyLayout), isDisplayed()))
        onView(withId(R.id.closeButton)).perform(click())
        onView(allOf(withId(R.id.transferMoneyLayout), not(isDisplayed())))
    }

    @Test
    fun addMoneyTest() {
        onView(withId(R.id.addMoneyButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), isDisplayed()))
        onView(withId(R.id.moneyEditText)).perform(replaceText("5"))
        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), not(isDisplayed())))
    }

    @Test
    fun cancelAddMoneyTest() {
        onView(withId(R.id.addMoneyButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), isDisplayed()))
        onView(withId(R.id.closeButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), not(isDisplayed())))
    }

    @Test
    fun retractMoneyTest() {
        onView(withId(R.id.getMoneyButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), isDisplayed()))
        onView(withId(R.id.moneyEditText)).perform(replaceText("5"))
        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), not(isDisplayed())))
    }

    @Test
    fun cancelRetractMoneyTest() {
        onView(withId(R.id.getMoneyButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), isDisplayed()))
        onView(withId(R.id.closeButton)).perform(click())
        onView(allOf(withId(R.id.modifyAccountLayout), not(isDisplayed())))
    }

}