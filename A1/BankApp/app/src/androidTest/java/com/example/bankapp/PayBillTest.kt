package com.example.bankapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.bankapp.presentation_layer.employee.ClientDetailsActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PayBillTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<ClientDetailsActivity>()

    @Before
    fun setUp() {
        onView(withId(R.id.payBillButton)).perform(click())
        onView(allOf(withId(R.id.payBillLayout), isDisplayed()))
    }


    @Test
    fun payGasBillTest() {
        onView(withId(R.id.gasLayout)).perform(click())
        onView(withId(R.id.gasEditText)).perform(replaceText("50"))

        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.payBillLayout), not(isDisplayed())))
    }

    @Test
    fun payElectricityBillTest() {
        onView(withId(R.id.electricityLayout)).perform(click())
        onView(withId(R.id.electricityEditText)).perform(replaceText("15"))

        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.payBillLayout), not(isDisplayed())))
    }

    @Test
    fun payWaterBillTest() {
        onView(withId(R.id.waterLayout)).perform(click())
        onView(withId(R.id.waterEditText)).perform(replaceText("30"))

        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.payBillLayout), not(isDisplayed())))
    }

    @Test
    fun payAllBillsTest() {
        onView(withId(R.id.waterLayout)).perform(click())
        onView(withId(R.id.waterEditText)).perform(replaceText("30"))

        onView(withId(R.id.gasLayout)).perform(click())
        onView(withId(R.id.gasEditText)).perform(replaceText("50"))

        onView(withId(R.id.electricityLayout)).perform(click())
        onView(withId(R.id.electricityEditText)).perform(replaceText("15"))

        onView(withId(R.id.doneButton)).perform(click())
        onView(allOf(withId(R.id.payBillLayout), not(isDisplayed())))
    }


}