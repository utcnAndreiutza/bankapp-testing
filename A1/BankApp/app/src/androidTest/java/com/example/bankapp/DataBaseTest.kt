package com.example.bankapp

import android.content.Context
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.example.bankapp.database_layer.*
import com.example.bankapp.domain_model_layer.Admin
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.domain_model_layer.Employee
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import java.io.IOException
import java.lang.NullPointerException
import java.util.*


@RunWith(AndroidJUnit4::class)
class DataBaseTest {
    private lateinit var adminDao: AdminDao
    private lateinit var employeeDao: EmployeeDao
    private lateinit var clientDao: ClientDao
    private lateinit var db: AppDatabase
    private lateinit var context:Context
    private lateinit var adminTest1: Admin
    private lateinit var adminTest2: Admin
    private lateinit var employeeTest1: Employee
    private lateinit var employeeTest2: Employee
    private lateinit var clientTest1: Client
    private lateinit var clientTest2: Client

    @Before
    fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().targetContext

        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        adminDao = db.getAdminDao()
        employeeDao = db.getEmployeeDao()
        clientDao = db.getClientDao()

        initDataBase()
    }

    private fun initDataBase() {
        adminTest1 = Admin(
            id =1,
            name = "Andreea Nitu",
            email = "admin@mail.com",
            password = context.resources.getString(R.string.pswdA),
            cnp = "2720101416881",
            adress = "Str. Sighisoarei nr120",
            phoneNumber = "0749366821"
        )
        adminTest2 = Admin(
            id= 2,
            name = "Blanka Nitu",
            email = "adminB@mail.com",
            password = context.resources.getString(R.string.pswdA),
            cnp = "2720101516841",
            adress = "Str. Garoafelor nr121",
            phoneNumber = "0769366921"
        )
        employeeTest1 =Employee(id= 1,name = "Lucica Hill", email = "employee1@mail.com", password = context.getString(R.string.pswd1), cnp = "2720101416241", adress="Str. Mihai Viteazu nr20", phoneNumber = "0749366921")
        employeeTest2 = Employee(id=2,name = "Marius Brass",email = "employee2@mail.com", password =context.getString(R.string.pswd2) , cnp = "1720101416641",adress="Str. Garoafelor nr21", phoneNumber = "0740066921")
        clientTest1 = Client(id=1,name = "WaterSystem", email = "WaterSystem@mail.com",cardId = "WaterSystem0001",phoneNumber = "0846255793",cnp = "0000000000001",amount = 200.0,creationDate = Date(),adress = "Str. Sighisoarei nr120")
        clientTest2 = Client(id=2,name = "GasSystem", email = "GasSystem@mail.com",cardId = "GasSystem0002",phoneNumber = "0846255733",cnp = "0000000000002",amount = 200.0,creationDate = Date(),adress = "Str. Sighisoarei nr220")
        adminDao.insertAdmin(adminTest1)
        adminDao.insertAdmin(adminTest2)
        employeeDao.insertEmployee(employeeTest1)
        employeeDao.insertEmployee(employeeTest2)
        clientDao.insertClient(clientTest1)
        clientDao.insertClient(clientTest2)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
    @Test
    fun readAdminList(){
        val adminList = adminDao.getAllAdmins()
        assertTrue(adminList.size == 2)
        assertEquals(adminList[0], adminTest1)
        assertEquals(adminList[1], adminTest2)
    }
    @Test
    fun readAdmin(){
        val admin = adminDao.getAdminByName(adminTest1.name)
        assertEquals(admin[0], adminTest1)
    }
    @Test
    fun readEmployee(){
        val employees = employeeDao.getAllEmployees()
        val employee = employeeTest1.id?.let { employeeDao.getEmployeeById(it) }
        assertEquals(employee, employeeTest1)
        assertEquals(employees[0], employeeTest1)
        assertTrue(employees.size == 2)
    }

    @Test
    fun readClient(){
        val clients = clientDao.getAllClients()
        val client = clientTest1.id?.let { clientDao.getClientById(it) }
        assertEquals(clients[0], clientTest1)
        assertEquals(client, clientTest1)
        assertTrue(clients.size == 2)
    }


    fun login() {
        val admin= adminDao.login(adminTest1.email,adminTest1.password)
        val employee= employeeDao.login(employeeTest1.email,employeeTest1.password)
        assertThat(employee, equalTo(employeeTest1))
        assertThat(admin, equalTo(adminTest1))
    }



    @Test
    fun updateEmployee(){
        val newName = "Ale"
        employeeDao.updateEmployee(employeeTest1.id,newName,employeeTest1.cnp,employeeTest1.adress,employeeTest1.phoneNumber,employeeTest1.email)
        val employee= employeeTest1.id?.let { employeeDao.getEmployeeById(it) }
        assertEquals(employee!!.name,newName)
    }

    @Test
    fun updateClient(){
        val newName = "Ale"
        clientDao.updateClient(clientTest1.id!!,newName,clientTest1.cnp,clientTest1.adress,clientTest1.phoneNumber,clientTest1.email,clientTest1.creationDate)
        val client= clientTest1.id?.let { clientDao.getClientById(it) }
        assertEquals(client!!.name,newName)
    }

    @Test
    fun addMoneyToAccount(){
        val money = 30.0
        clientTest1.id?.let { clientDao.addMoney(it,money) }
        val client= clientTest1.id?.let { clientDao.getClientById(it) }
        assertEquals(client!!.amount, clientTest1.amount + money,0.0)
    }


    @Test
    @Throws(NullPointerException::class)
    fun deleteClient(){
        try {
            clientTest1.id?.let {
                clientDao.deleteClientWithId(it)
                val x = clientDao.getClientById(it)
                assertEquals(x.id, it)
            }
        }catch (e: NullPointerException){
            return
        }
        fail()
    }

    @Test
    fun deleteEmployee(){
        try {
            employeeTest1.id?.let {
                employeeDao.deleteEmployeeWithId(it)
                val x = employeeDao.getEmployeeById(it)
                assertEquals(x.id, it)
            }
        }catch (e: NullPointerException){
            return
        }
        fail()
    }



}
