package com.example.bankapp

import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.bankapp.presentation_layer.LoginActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AddEmployeeTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<LoginActivity>()
    lateinit var  decorView : View

    @Before
    fun setUp() {
        activityScenarioRule.scenario.onActivity {
            decorView = it.window.decorView
        }
        //login and click on add button
        onView(withId(R.id.userEmail)).perform(replaceText("admin@mail.com"))
        onView(withId(R.id.userPassword)).perform(replaceText("admin"))
        onView(withId(R.id.loginButton)).perform(click())
        onView(withId(R.id.addEmployee)).perform(click())
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.bankapp", appContext.packageName)
    }

    @Test
    fun addEmployeeTestEmptyFields() {
        //check add information layout is displayed
        onView(allOf(withId(R.id.infoLayout), isDisplayed()))
        //check if layout is not closed yet because the fields are not complete after add button was pressed
        onView(withId(R.id.addEmpoyeeButtonDialog)).perform(click())
        onView(allOf(withId(R.id.infoLayout), isDisplayed()))
    }

    @Test
    fun invalidEmailAddEmployeeTest() {
        onView(withId(R.id.nameEditText)).perform(replaceText("Mariana Lizer"))
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText("212345434543"))
        onView(withId(R.id.adressEditText)).perform(replaceText("Str. Clujului, nr 112"))
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText("0758677965"))
        //set invalid email
        onView(withId(R.id.emailEditText)).perform(replaceText("invalidEmail")).perform(closeSoftKeyboard())
        onView(withId(R.id.addEmpoyeeButtonDialog)).perform(click())
        //check if layout is not closed yet (add was not done)
        onView(allOf(withId(R.id.infoLayout), isDisplayed()))
    }

    @Test
    fun invalidCNPAddEmployeeTest() {
        onView(withId(R.id.nameEditText)).perform(replaceText("Mariana Lizer"))
        //set invalid cnp
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText("212"))
        onView(withId(R.id.adressEditText)).perform(replaceText("Str. Clujului, nr 112"))
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText("0758677965"))
        onView(withId(R.id.emailEditText)).perform(replaceText("MarianaLizer@gmail.com")).perform(closeSoftKeyboard())
        onView(withId(R.id.addEmpoyeeButtonDialog)).perform(click())
        //check if layout is not closed yet (add was not done)
        onView(allOf(withId(R.id.infoLayout), isDisplayed()))
    }

    @Test
    fun successAddEmployeeTest() {
        //insert information
        onView(withId(R.id.nameEditText)).perform(replaceText("Mariana Lizer"))
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText("212345434543"))
        onView(withId(R.id.adressEditText)).perform(replaceText("Str. Clujului, nr 112"))
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText("0758677965"))
        onView(withId(R.id.emailEditText)).perform(replaceText("MarianaLizer@gmail.com")).perform(closeSoftKeyboard())
        onView(withId(R.id.addEmpoyeeButtonDialog)).perform(click())
        onView(allOf(withId(R.id.infoLayout), not(isDisplayed())))
    }



}