package com.example.bankapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.bankapp.presentation_layer.admin.EmployeeDetailsActivity
import com.example.bankapp.presentation_layer.employee.ClientDetailsActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.*
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class EditClientTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<ClientDetailsActivity>()
    private var doneWithSuccess = true
    private var isDelete = false

    @Before
    fun setUp() {
        onView(allOf(withId(R.id.clientLayout), isDisplayed()))
    }

    @After
    fun tearDown() {
        if(!isDelete){
            onView(withId(R.id.checkDone)).perform(click())
        }

        if(doneWithSuccess){
            onView(allOf(withId(R.id.clientLayout), not(isDisplayed())))
        }else{
            onView(allOf(withId(R.id.clientLayout), isDisplayed()))
        }
    }

    @Test
    fun testA_editAll() {
        onView(withId(R.id.nameEditText)).perform(replaceText("Mariana Lizer"))
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText(getGeneratedCNP()))
        onView(withId(R.id.adressEditText)).perform(replaceText("Str. Clujului, nr 112"))
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText(getRandomPhoneNumber()))
        onView(withId(R.id.emailEditText)).perform(replaceText("MarianaLizer@gmail.com")).perform(closeSoftKeyboard())
    }


    @Test
    fun testB_editName() {
        onView(withId(R.id.nameEditText)).perform(replaceText("new name for test"))
    }

    @Test
    fun testC_editAddress() {
        onView(withId(R.id.adressEditText)).perform(replaceText("Str. Clujului, nr 112"))
    }

    @Test
    fun testD_successEditCNP() {
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText(getGeneratedCNP()))
    }

    @Test
    fun testE_failEditCNP() {
        onView(withId(R.id.cnpNumberEditText)).perform(replaceText("123"))
        doneWithSuccess = false
    }

    @Test
    fun testF_successEditPhoneNumber() {
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText(getRandomPhoneNumber()))
    }

    @Test
    fun testG_failEditPhoneNumber() {
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText("123"))
        doneWithSuccess = false
    }

    @Test
    fun testH_successEditEmail() {
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText(getRandomEmail()))
    }

    @Test
    fun testI_failEditEmail() {
        onView(withId(R.id.phoneNumberEditText)).perform(replaceText("abc"))
        doneWithSuccess = false
    }

    @Test
    fun testJ_deleteClient() {
        onView(withId(R.id.deleteClient)).perform(click())
        isDelete = true
    }

    private fun getGeneratedCNP() : String {
        val allowedChars = ('0'..'9')
        var generatedCNP = (1..12)
            .map { allowedChars.random() }
            .joinToString("")

        val firstAllowedChar = listOf('1','2')
        val gender = (1..1)
            .map { firstAllowedChar.random() }
            .joinToString("")

        generatedCNP = gender + generatedCNP
        return generatedCNP
    }

    private fun getRandomPhoneNumber() : String {
        val allowedChars = ('0'..'9')
        val randomPhoneNumber = (1..8)
            .map { allowedChars.random() }
            .joinToString("")
        return "07$randomPhoneNumber"
    }

    private fun getRandomEmail() : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        val randomString =  (1..10)
            .map { allowedChars.random() }
            .joinToString("")
        return  "$randomString@mail.com"
    }


}