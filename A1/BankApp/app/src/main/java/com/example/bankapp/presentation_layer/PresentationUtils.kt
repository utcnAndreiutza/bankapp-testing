package com.example.bankapp.presentation_layer

import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast

inline fun <reified T> Activity.startActivity(bundle: Bundle = Bundle()){
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    startActivity(intent)
}

fun increaseViewSize(view: View) {
    val valueAnimator = ValueAnimator.ofInt(view.measuredHeight, view.measuredHeight+200)
    valueAnimator.duration = 5000L
    valueAnimator.addUpdateListener {
        val animatedValue = valueAnimator.animatedValue as Int
        val layoutParams = view.layoutParams
        layoutParams.height = animatedValue
        view.layoutParams = layoutParams
    }
    valueAnimator.start()
}

fun showToast(context: Context,toastMessage: String){
    Toast.makeText(context,toastMessage, Toast.LENGTH_LONG).show()
}