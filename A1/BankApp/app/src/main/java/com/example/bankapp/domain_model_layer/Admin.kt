package com.example.bankapp.domain_model_layer

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

@Entity(tableName = "admin")
data class Admin(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    val name: String,
    val cnp: String,
    val adress: String,
    val phoneNumber: String,
    val email: String,
    val password: String
)