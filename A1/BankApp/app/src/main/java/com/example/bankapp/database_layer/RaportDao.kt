package com.example.bankapp.database_layer
import androidx.room.*
import com.example.bankapp.domain_model_layer.Raport
import java.util.Date

@Dao
interface RaportDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun logEvent(historyDao: Raport)

    @Query("SELECT * FROM history WHERE  employeeId== :employeeId and creationDate>=:date1 and creationDate<=:date2 ORDER BY creationDate DESC")
    fun getHistoryOfEmployee(employeeId: Int, date1: Date, date2: Date): List<Raport>
}