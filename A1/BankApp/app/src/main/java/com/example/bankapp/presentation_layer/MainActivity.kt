package com.example.bankapp.presentation_layer

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.InitBusinessClass
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private lateinit var initBusinessClass :InitBusinessClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        initBusinessClass = InitBusinessClass(applicationContext)
        initBusinessClass.initDatabase()
        startAnimation()
    }

    @SuppressLint("CheckResult")
    private fun startAnimation() {
        Completable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe {
                startActivity<LoginActivity>()
            }


    }

}
