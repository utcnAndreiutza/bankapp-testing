package com.example.bankapp.presentation_layer.admin

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.AdminBusinessClass
import com.example.bankapp.business_layer.getDateFromString
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.domain_model_layer.Employee
import com.example.bankapp.presentation_layer.showToast
import com.itextpdf.text.Document
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfWriter
import kotlinx.android.synthetic.main.activity_employee_details.*
import kotlinx.android.synthetic.main.create_raport_dialog.view.*
import kotlinx.android.synthetic.main.user_details.*
import java.io.FileOutputStream

class EmployeeDetailsActivity : AppCompatActivity() {
    private lateinit var adminBusinessClass :AdminBusinessClass
    private var employeeID: Int = 1
    private val STORAGE_CODE: Int = 100

    companion object {
        val EMPLOYEE_DETAILS_KEY = "EmployeeDetailsActivity.key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details)
        adminBusinessClass = AdminBusinessClass(
            AppDatabase.getAppDataBase(applicationContext)?.getAdminDao(), AppDatabase.getAppDataBase(applicationContext)?.getEmployeeDao(),
            AppDatabase.getAppDataBase(applicationContext)?.getRaportDao())
        createRaportButton.setOnClickListener {
            showCreateRaportDialog()
        }
        deleteEmployeeButton.setOnClickListener {
            adminBusinessClass.deleteEmployeeWithId(employeeID)
            finish()
        }

        checkDone.setOnClickListener {
            val name = nameEditText.text.toString()
            val cnp = cnpNumberEditText.text.toString()
            val adress = adressEditText.text.toString()
            val phoneNumber = phoneNumberEditText.text.toString()
            val email = emailEditText.text.toString()
            val currentEmployee = Employee(
                id = employeeID,
                name = name,
                cnp = cnp,
                adress = adress,
                phoneNumber = phoneNumber,
                email = email,
                password = ""
            )
            if (adminBusinessClass.isEmployeeDataValid(currentEmployee)) {
                adminBusinessClass.updateEmployee(currentEmployee)
                finish()
            } else {
                showToast(applicationContext,"Update error. Employee data invalid.\n 1)Check empty fields\n 2)Make sure that CNP and email are correct.")
            }
        }
    }

    private fun showCreateRaportDialog() {

        val dialogBuilder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.create_raport_dialog, null)
        dialogBuilder.setView(view)

        val endDateEditText = view.endDate
        val startDateEditText = view.startDate

        view.closeButton.setOnClickListener {
            dialogBuilder.cancel()
        }

        view.doneButton.setOnClickListener {
            val endDate = endDateEditText.text.toString()
            val startDate = startDateEditText.text.toString()
            if (adminBusinessClass.areRaportDatesValid(startDate, endDate)) {
                adminBusinessClass.getReportForPeriod(
                    employeeID,
                    getDateFromString(startDate),
                    getDateFromString(endDate)
                )
            } else {
                showToast(applicationContext,"1)Please respect date format: " + getString(R.string.date_format) + "2) startDate < endDate!")
            }
        }
        dialogBuilder.show()
    }

    override fun onStart() {
        super.onStart()
        val id = intent?.getIntExtra(EMPLOYEE_DETAILS_KEY, 1)
        if (id != null) {
            employeeID = id
            adminBusinessClass.getEmployeeWithId(id)
        }
        adminBusinessClass.employee.observe(this, Observer { employee ->
            nameEditText.setText(employee.name)
            cnpNumberEditText.setText(employee.cnp)
            adressEditText.setText(employee.adress)
            phoneNumberEditText.setText(employee.phoneNumber)
            emailEditText.setText(employee.email)
        })

        adminBusinessClass.raportList.observe(this, Observer { raportList ->
            raportList?.apply { generateRaport() }
        })
    }

    override fun onStop() {
        super.onStop()
        adminBusinessClass.employee.removeObservers(this)
        adminBusinessClass.raportList.removeObservers(this)
    }

    private fun generateRaport() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                requestPermissions(permissions, STORAGE_CODE)
            } else {
                savePdf()
            }
        } else {
            savePdf()
        }
    }


    private fun savePdf() {
        val document = Document()
        val textList = adminBusinessClass.getRaportAsStringList()
        val pdfName = textList[0]
        val pdfPath = Environment.getExternalStorageDirectory().toString() + "/" + pdfName + ".pdf"
        try {
            PdfWriter.getInstance(document, FileOutputStream(pdfPath))
            document.open()
            document.addAuthor("Andreea Nitu")
            for (textLine in textList)
                document.add(Paragraph(textLine))
            document.close()
            showToast(applicationContext,"$pdfName.pdf\nis saved to\n$pdfPath")
        } catch (e: Exception) {
            e.message?.let { showToast(applicationContext,it) }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            STORAGE_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    savePdf()
                } else {
                    Toast.makeText(this, "Permission denied...!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
