package com.example.bankapp.domain_model_layer

import android.os.Parcel
import android.os.Parcelable


interface AppUserType{
    companion object{
        const val  ADMIN_TYPE =1
        const val  EMPLOYEE_TYPE =2
    }
}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
data class DisplayableUser (
    val userType:Int,
    val id: Int? = null,
    val name: String,
    val cnp: String,
    val adress: String,
    val phoneNumber: String,
    val email: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(userType)
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(cnp)
        parcel.writeString(adress)
        parcel.writeString(phoneNumber)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DisplayableUser> {
        override fun createFromParcel(parcel: Parcel): DisplayableUser {
            return DisplayableUser(parcel)
        }

        override fun newArray(size: Int): Array<DisplayableUser?> {
            return arrayOfNulls(size)
        }
    }
}