package com.example.bankapp.business_layer.classes

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.bankapp.business_layer.interfaces.LoginBusinessInterface
import com.example.bankapp.business_layer.isEmailValidUtils
import com.example.bankapp.database_layer.AdminDao
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.database_layer.EmployeeDao
import com.example.bankapp.domain_model_layer.AppUserType
import com.example.bankapp.domain_model_layer.DisplayableUser
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginBusinessClass(private val adminDao: AdminDao?, private val employeeDao:EmployeeDao?) : LoginBusinessInterface {

    private val _userType = MutableLiveData<DisplayableUser>()
    val userType: LiveData<DisplayableUser> = _userType


    override fun login(userEmail: String, userPassword: String) {
        try {
            val employee = employeeDao?.login(userEmail, userPassword)
            if (employee == null) {
                Log.d("ASDF", "No employee")
                loginAdmin(userEmail, userPassword)
            } else {
                _userType.postValue(employee.id?.let {
                    DisplayableUser(
                        AppUserType.EMPLOYEE_TYPE,
                        employee.id,
                        employee.name,
                        employee.cnp,
                        employee.adress,
                        employee.phoneNumber,
                        employee.email
                    )
                })
            }
        }catch (e : Throwable){
            _userType.postValue(null)
        }
    }

    override fun isEmailValid(userEmail: String): Boolean {
        return isEmailValidUtils(userEmail)
    }

    override fun isPasswordValid(userPassword: String): Boolean {
        return userPassword.length >= 5
    }

    private fun loginAdmin(userEmail: String, userPassword: String) {

            val admin= adminDao?.login(userEmail, userPassword)
            Log.d("ASDF", admin?.name)
            if (admin == null) {
                Log.d("ASDF", "No admin")
                _userType.postValue(null)
            } else
                _userType.postValue(admin.id?.let {
                    DisplayableUser(
                        AppUserType.ADMIN_TYPE,
                        admin.id,
                        admin.name,
                        admin.cnp,
                        admin.adress,
                        admin.phoneNumber,
                        admin.email
                    )
                })

    }

}
