package com.example.bankapp.business_layer.classes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.bankapp.business_layer.interfaces.EmployeeRequestsInterface
import com.example.bankapp.business_layer.isEmailValidUtils
import com.example.bankapp.database_layer.*
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.domain_model_layer.Raport
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class EmployeeBusinessClass(private val clientDao: ClientDao?, private val raportDao: RaportDao?): EmployeeRequestsInterface {

    private val _client = MutableLiveData<Client>()
    val client: LiveData<Client>
        get(){
            return _client
        }
    private val _clientList = MutableLiveData<List<Client>>()
    val clientList: LiveData<List<Client>>
        get(){
            return _clientList
        }


    override fun getAllClients() {
        Observable.fromCallable {
            clientDao?.getAllClients()
        }.doOnNext { list ->
            if(list!=null)
                _clientList.postValue(list)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun insertClient(client: Client) {
        Observable.fromCallable {
            clientDao?.apply {
                insertClient(client)
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertRaport(employeeID: Int,description: String){
        Thread{
            raportDao?.logEvent(Raport(employeeId = employeeID,creationDate = Date(),description = description))
        }.start()
    }

    fun getClientFromList(position: Int): Client? {
        return _clientList.value?.get(position)
    }

    override fun getClientWithId(id: Int) {
        Observable.fromCallable {
            clientDao?.getClientById(id)
        }.doOnNext {client->
            _client.postValue(client)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun updateClient(client: Client) {
       Thread{ client.id?.let { clientDao?.updateClient(it,client.name,client.cnp,client.adress,client.phoneNumber,client.email,client.creationDate) } }.start()
    }

    override fun deleteClientWithId(id: Int) {
        Thread {
            clientDao?.deleteClientWithId(id)
        }.start()

    }

    override fun addMoney(id: Int, amount: Double) {
        clientDao?.addMoney(id,amount)
    }

    override fun extractMoneyFromAccount(id: Int, amount: Double) {
        clientDao?.addMoney(id,amount *(-1))
    }

    override fun transactionBetweenAccounts(
        clientID: Int,
        toAccountNumber: String,
        amount: Double
    ) {
        extractMoneyFromAccount(clientID, amount)
        val s= toAccountNumber.split("ID")
        val toClientID = s[0].toInt()
        addMoney(toClientID, amount)
    }



    fun isClientDataValid(client: Client): Boolean {
        val notEmptyFields = client.name.isNotBlank() && client.adress.isNotBlank()
        return isEmailValidUtils(client.email) && client.cnp.length==13 && client.phoneNumber.length==10 && notEmptyFields
    }

    fun isAmountValid(amountValue: Double): Boolean {
        return amountValue>0
    }

    fun areAmountsValid(waterText: String, gasText: String, electricityText: String): Boolean {
        try{
            val waterAmount = waterText.toDouble()
            val gasAmount =gasText.toDouble()
            val electricityAmount =electricityText.toDouble()
            return waterAmount>=0 && gasAmount>=0 && electricityAmount>=0
        }catch (e: Throwable){
            return false
        }
    }

    fun generateCardId(): String{
        return "ID"+UUID.randomUUID().toString().take(5)
    }
}

