package com.example.bankapp.domain_model_layer

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

@Entity(tableName = "employee")
data class Employee(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    var name: String,
    var cnp: String,
    var adress: String,
    var phoneNumber: String,
    var email: String,
    val password: String
)