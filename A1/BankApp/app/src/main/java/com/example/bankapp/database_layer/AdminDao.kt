package com.example.bankapp.database_layer

import androidx.room.*
import com.example.bankapp.domain_model_layer.Admin

@Dao
interface AdminDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAdmin(admin: Admin)

    @Query("DELETE FROM admin")
    fun deleteAllAdmins()

    @Query("SELECT * FROM admin WHERE name == :name")
    fun getAdminByName(name: String): List<Admin>

    @Query("SELECT * FROM admin")
    fun getAllAdmins(): List<Admin>

    @Query("SELECT * FROM admin where email == :userEmail and password == :userPassword ")
    fun login(userEmail: String, userPassword: String): Admin
}