package com.example.bankapp.presentation_layer

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.LoginBusinessClass
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.domain_model_layer.AppUserType
import com.example.bankapp.presentation_layer.admin.AdminActivity
import com.example.bankapp.presentation_layer.employee.EmployeeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private lateinit var loginBusinessClass: LoginBusinessClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginBusinessClass = LoginBusinessClass(AppDatabase.getAppDataBase(applicationContext)?.getAdminDao(), AppDatabase.getAppDataBase(applicationContext)?.getEmployeeDao())
        loginBusinessClass.userType.observe(this, Observer { appUser ->
            if(appUser != null){
                when (appUser.userType) {
                    AppUserType.ADMIN_TYPE -> {
                        val bundle = Bundle()
                        bundle.putParcelable(AdminActivity.ADMIN_USER_KEY,appUser)
                        startActivity<AdminActivity>(bundle)
                    }
                    AppUserType.EMPLOYEE_TYPE -> {
                        val bundle = Bundle()
                        bundle.putParcelable(EmployeeActivity.EMPLOYEE_USER_KEY,appUser)
                        startActivity<EmployeeActivity>(bundle)
                    }
                }
            }else{
                showUserNotFoundMessage()
            }

        })

        loginButton.setOnClickListener {
            val email = userEmail.text.toString()
            val password = userPassword.text.toString()
            if (!loginBusinessClass.isEmailValid(email)) {
                showEmailErrorMessage()
            } else if (!loginBusinessClass.isPasswordValid(password)) {
                showPasswordErrorMessage()
            } else
                loginBusinessClass.login(email, password)
        }


    }

    private fun showUserNotFoundMessage() {
        Log.d("asdf",getString(R.string.user_not_found))
       Toast.makeText(this.baseContext, getString(R.string.user_not_found), Toast.LENGTH_LONG).show()
    }

    private fun showPasswordErrorMessage() {
        Log.d("asdf",getString(R.string.invalid_password))
        Toast.makeText(this.baseContext, getString(R.string.invalid_password), Toast.LENGTH_LONG).show()
    }

    private fun showEmailErrorMessage() {
        Log.d("asdf",getString(R.string.invalid_email))
        Toast.makeText(this.baseContext, getString(R.string.invalid_email), Toast.LENGTH_LONG).show()
    }
}

