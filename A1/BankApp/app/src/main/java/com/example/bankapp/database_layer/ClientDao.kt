package com.example.bankapp.database_layer

import androidx.room.*
import com.example.bankapp.domain_model_layer.Client
import java.util.*

@Dao
interface ClientDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertClient(client: Client)

    @Query("DELETE FROM client WHERE id ==:id")
    fun deleteClientWithId(id: Int)

    @Query("DELETE FROM client")
    fun deleteAllClients()

    @Query("SELECT * FROM client WHERE id == :id")
    fun getClientById(id: Int): Client

    @Query("SELECT * FROM client")
    fun getAllClients(): List<Client>

    @Query("UPDATE client SET amount= amount+ :amount WHERE id=:id")
    fun addMoney(id:Int, amount:Double)

    @Query("UPDATE client SET name=:name, cnp=:cnp, adress= :adress, phoneNumber= :phoneNumber, email= :email, creationDate= :creationDate WHERE id=:id")
    fun updateClient(id:Int,
                     name:String,
                     cnp:String,
                     adress:String,
                     phoneNumber:String,
                     email:String,
                     creationDate:Date)
}