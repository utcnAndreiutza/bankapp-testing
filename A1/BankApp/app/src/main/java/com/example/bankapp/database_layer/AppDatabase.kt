package com.example.bankapp.database_layer

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.bankapp.domain_model_layer.Admin
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.domain_model_layer.Employee
import com.example.bankapp.domain_model_layer.Raport
import com.example.bankapp.presentation_layer.MainActivity

@Database(entities = [Admin::class, Employee::class, Client::class, Raport::class], version = 2,exportSchema = true)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getAdminDao(): AdminDao
    abstract fun getEmployeeDao(): EmployeeDao
    abstract fun getClientDao(): ClientDao
    abstract fun getRaportDao(): RaportDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        "BankApp"
                    ).fallbackToDestructiveMigration().allowMainThreadQueries().build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}