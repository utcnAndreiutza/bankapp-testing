package com.example.bankapp.business_layer.classes

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.bankapp.business_layer.getDateFormat
import com.example.bankapp.business_layer.getDateFromString
import com.example.bankapp.business_layer.interfaces.AdminRequestsInterface
import com.example.bankapp.business_layer.isEmailValidUtils
import com.example.bankapp.database_layer.AdminDao
import com.example.bankapp.database_layer.EmployeeDao
import com.example.bankapp.database_layer.RaportDao
import com.example.bankapp.domain_model_layer.Employee
import com.example.bankapp.domain_model_layer.Raport
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class AdminBusinessClass(private val adminDao: AdminDao?, private val employeeDao:EmployeeDao?, private val raportDao:RaportDao?) : AdminRequestsInterface {

    private val _employee = MutableLiveData<Employee>()
    val employee: LiveData<Employee>
        get() {
            return _employee
        }

    private val _raportList = MutableLiveData<List<Raport>>()
    val raportList: LiveData<List<Raport>>
        get() {
            return _raportList
        }

    private val _employeesList = MutableLiveData<List<Employee>>()
    val employeesList: LiveData<List<Employee>>
        get() {
            return _employeesList
        }


    override fun getAllAdmins() {
        Observable.fromCallable {
            adminDao?.getAllAdmins()
        }.doOnNext { list ->
            var finalString = ""
            list?.map { finalString += it.name + " - " + it.id + "\n" }
            Log.d("asdf", finalString + "\n")
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun getAllEmployees() {
        Observable.fromCallable {
            employeeDao?.getAllEmployees()
        }.doOnNext { list ->
            if (list != null)
                _employeesList.postValue(list)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun getEmployeeWithId(id: Int) {
        Observable.fromCallable {
            employeeDao?.getEmployeeById(id)
        }.doOnNext { employee ->
            _employee.postValue(employee)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }


    override fun updateEmployee(employee: Employee) {
        Thread {
            employeeDao?.updateEmployee(
                employee.id,
                employee.name,
                employee.cnp,
                employee.adress,
                employee.phoneNumber,
                employee.email
            )
        }.start()
    }

    override fun insertEmployee(employee: Employee) {
        Observable.fromCallable {
            employeeDao?.apply {
                insertEmployee(employee)
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteEmployeeWithId(id: Int) {
        Thread {
            employeeDao?.deleteEmployeeWithId(id)
        }.start()


    }

    override fun getReportForPeriod(employeeId: Int, startDate: Date, endDate: Date) {
        Observable.fromCallable {
            raportDao?.getHistoryOfEmployee(employeeId, startDate, endDate)
        }.doOnNext { raportList ->
            _raportList.postValue(raportList)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun getEmployeeOnPosition(position: Int): Int? {
        val employee = _employeesList.value?.get(position)
        return employee?.id
    }

    fun isEmployeeDataValid(employee: Employee): Boolean {
        val notEmptyFields = employee.name.isNotBlank() && employee.adress.isNotBlank()
        return isEmailValidUtils(employee.email) && employee.cnp.length == 13 && employee.phoneNumber.length == 10 && notEmptyFields
    }

    fun areRaportDatesValid(startDate: String, endDate: String): Boolean {
        try {
            val date1 = getDateFromString(startDate)
            val date2 = getDateFromString(endDate)
            if (date1 > date2) return false
        } catch (e: Throwable) {
            return false
        }
        return true
    }

    fun getRaportAsStringList(): ArrayList<String> {
       val strings: ArrayList<String> = ArrayList()
        val raports = raportList.value
        if (raports != null) {
            strings.add("Employee id:" + raports[0].employeeId.toString()+ "\n")
            for(raport in raports){
                val data = getDateFormat(raport.creationDate)
                strings.add(data+ ":"+ raport.description+"\n")
            }
        }
        return strings
    }
}