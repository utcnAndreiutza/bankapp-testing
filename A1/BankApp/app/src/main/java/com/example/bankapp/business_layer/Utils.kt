package com.example.bankapp.business_layer

import androidx.core.util.PatternsCompat
import java.text.SimpleDateFormat
import java.util.*


fun isEmailValidUtils(email: String) : Boolean{
    if(email.contains('@')){
        return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()
    }
    return false
}

fun getDateFormat(date: Date):String{
    val pattern = "HH:mm, dd.MM.yyyy"
    val simpleDateFormat = SimpleDateFormat(pattern)
    return simpleDateFormat.format(date)
}

fun getDateFromString(date:String):Date{
    val pattern = "HH:mm, dd.MM.yyyy"
    val simpleDateFormat = SimpleDateFormat(pattern)
    return simpleDateFormat.parse(date)
}
