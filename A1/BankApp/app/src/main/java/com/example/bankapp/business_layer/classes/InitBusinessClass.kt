package com.example.bankapp.business_layer.classes

import android.content.Context
import android.util.Log
import com.example.bankapp.R
import com.example.bankapp.database_layer.AdminDao
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.database_layer.ClientDao
import com.example.bankapp.database_layer.EmployeeDao
import com.example.bankapp.domain_model_layer.Admin
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.domain_model_layer.Employee
import com.example.bankapp.presentation_layer.MainActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

class InitBusinessClass(val context: Context){
    private var db: AppDatabase? = AppDatabase.getAppDataBase(context)
    private var adminDao: AdminDao? = db?.getAdminDao()
    private var employeeDao: EmployeeDao? = db?.getEmployeeDao()
    private var clientDao: ClientDao? = db?.getClientDao()


    fun initDatabase(){
        initEmployeeList(getEmployeeList())
        initAdminList(getAdminsList())
        initClientList(getClientList())
    }

    private fun getEmployeeList():ArrayList <Employee> {
        val employeeList: ArrayList <Employee> = ArrayList()
        val pswd1 =context.getString(R.string.pswd1)
        val pswd2 =context.getString(R.string.pswd2)
        val pswd3 =context.getString(R.string.pswd3)
        employeeList.add(Employee(name = "Lucica Hill", email = "employee1@mail.com", password = pswd1, cnp = "2720101416241", adress="Str. Mihai Viteazu nr20", phoneNumber = "0749366921"))
        employeeList.add(Employee(name = "Marius Brass",email = "employee2@mail.com", password = pswd2, cnp = "1720101416641",adress="Str. Garoafelor nr21", phoneNumber = "0740066921"))
        employeeList.add(Employee(name = "Andreea Lirey",email = "employee3@mail.com", password = pswd3,cnp = "2720101576241",adress="Str. Sighisoarei nr22", phoneNumber = "0749300921"))
        return employeeList
    }

    private fun getAdminsList(): ArrayList <Admin>{
        val adminList: ArrayList <Admin> = ArrayList()
        val pswd =context.getString(R.string.pswdA)
        val pswdB =context.getString(R.string.pswdB)
        adminList.add(Admin(name = "Andreea Nitu",email = "admin@mail.com", password = pswd, cnp = "2720101416881", adress="Str. Sighisoarei nr120", phoneNumber = "0749366821"))
        adminList.add(Admin(name = "Blanka Nitu",email = "adminB@mail.com", password = pswdB, cnp = "2720101516841", adress="Str. Garoafelor nr121", phoneNumber = "0769366921"))
        return adminList
    }

    private fun getClientList(): ArrayList<Client>{
        val clientList = ArrayList<Client>()
        clientList.add(Client(name = "WaterSystem", email = "WaterSystem@mail.com",cardId = "WaterSystem0001",phoneNumber = "0846255793",cnp = "0000000000001",amount = 200.0,creationDate = Date(),adress = "Str. Sighisoarei nr120"))
        clientList.add(Client(name = "GasSystem", email = "GasSystem@mail.com",cardId = "GasSystem0002",phoneNumber = "0846255733",cnp = "0000000000002",amount = 200.0,creationDate = Date(),adress = "Str. Sighisoarei nr220"))
        clientList.add(Client(name = "ElectricitySystem", email = "ElectricitySystem@mail.com",cardId = "ElectricitySystem0003",phoneNumber = "0846275793",cnp = "0000000000003",amount = 200.0,creationDate = Date(),adress = "Str. Sighisoarei nr190"))
        return clientList
    }
    fun initEmployeeList(employees: List<Employee>) {
        Observable.fromCallable {
            employeeDao?.getAllEmployees()
        }.doOnNext { employeeList ->
            employeeList?.apply {
                if (employeeList.isEmpty()) {
                    Log.d("asdf", "empty employee\n")
                    employeeDao?.apply {
                        for (employee in employees)
                            insertEmployee(employee)
                    }
                } else {
                    Log.d("asdf", "Not empty employee\n")
                }
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun initAdminList(admins: List<Admin>) {
        Observable.fromCallable {
            adminDao?.getAllAdmins()
        }.doOnNext { adminsList ->
            adminsList?.apply {
                if (adminsList.isEmpty()) {
                    Log.d("asdf", "empty admin\n")
                    adminDao?.apply {
                        for (admin in admins)
                            insertAdmin(admin)
                    }
                } else {
                    Log.d("asdf", "Not empty admin\n")
                }
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun initClientList(initClientList: ArrayList<Client>) {
        Observable.fromCallable {
            clientDao?.getAllClients()
        }.doOnNext { clientList ->
            clientList?.apply {
                if (clientList.isEmpty()) {
                    Log.d("asdf", "empty clients\n")
                    clientDao?.apply {
                        for (client in initClientList)
                            insertClient(client)
                    }
                } else {
                    Log.d("asdf", "Not empty clients\n")
                }
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    /** !!! after running this function, you must increase the version number from AppDatabase class in code**/
    fun eraseDatabase() {
        AppDatabase.destroyDataBase()
        AppDatabase.getAppDataBase(context)
    }

}
