package com.example.bankapp.business_layer.interfaces

import com.example.bankapp.domain_model_layer.Employee
import java.util.*

interface AdminRequestsInterface {
    fun getAllAdmins()
    fun getAllEmployees()
    fun getEmployeeWithId(id:Int)
    fun updateEmployee(employee: Employee)
    fun insertEmployee(employee: Employee)
    fun deleteEmployeeWithId(id:Int)
    fun getReportForPeriod(employeeId:Int,startDate: Date, endDate: Date)

}