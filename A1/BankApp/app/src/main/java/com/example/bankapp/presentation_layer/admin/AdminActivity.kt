package com.example.bankapp.presentation_layer.admin

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.AdminBusinessClass
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.domain_model_layer.DisplayableUser
import com.example.bankapp.domain_model_layer.Employee
import com.example.bankapp.presentation_layer.showToast
import com.example.bankapp.presentation_layer.startActivity
import kotlinx.android.synthetic.main.activity_admin.*
import kotlinx.android.synthetic.main.user_details.view.*

class AdminActivity : AppCompatActivity() {
    private lateinit var adminBusinessClass:AdminBusinessClass
    private val currentUser : DisplayableUser by lazy{
        intent.getParcelableExtra<DisplayableUser>(ADMIN_USER_KEY)
    }

    companion object{
        const val ADMIN_USER_KEY = "AdminActivity.user.key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adminBusinessClass = AdminBusinessClass(AppDatabase.getAppDataBase(applicationContext)?.getAdminDao(), AppDatabase.getAppDataBase(applicationContext)?.getEmployeeDao(),AppDatabase.getAppDataBase(applicationContext)?.getRaportDao())
        adminBusinessClass.getAllEmployees()
        setContentView(R.layout.activity_admin)
        welcomeText.text = String.format(getString(R.string.welcome_user),currentUser.name)
        logOutButton.setOnClickListener{
            finish()
        }

        adminBusinessClass.employeesList.observe(this, Observer {employeeList->
            setListOfEmployees(employeeList)
        })

        addEmployee.setOnClickListener{
            addEmplployeeDialog()
        }
    }
    private fun addEmplployeeDialog(){
        val dialogBuilder = AlertDialog.Builder(this).create()
        val view= LayoutInflater.from(this).inflate(R.layout.user_details,null)
        view.setBackgroundResource(R.drawable.backgroud)
        view.addEmpoyeeButtonDialog.visibility = View.VISIBLE
        dialogBuilder.setView(view)
        val name=view.nameEditText
        val cnp=view.cnpNumberEditText
        val adress=view.adressEditText
        val phoneNumber=view.phoneNumberEditText
        val email=view.emailEditText
        view.addEmpoyeeButtonDialog.setOnClickListener{
            val employee = Employee(name=name.text.toString(),cnp=cnp.text.toString(),adress = adress.text.toString(),phoneNumber = phoneNumber.text.toString(),email = email.text.toString(),password = "NewUser")
            if(adminBusinessClass.isEmployeeDataValid(employee)){
                adminBusinessClass.insertEmployee(employee)
                dialogBuilder.cancel()
                adminBusinessClass.getAllEmployees()
            }else{
                showToast(applicationContext, getString(R.string.invalid_data_to_add_employee))
            }
        }
        dialogBuilder.show()
    }

    private fun setListOfEmployees(employeesList:List<Employee>) {
        val list = employeesList.map{ it.name}
        val adapter = ArrayAdapter(this, R.layout.employee_listview_layout, list)
        employeeList.adapter = adapter
        employeeList.setOnItemClickListener { _, _, position, _ ->
            val bundle = Bundle()
            val currentId = adminBusinessClass.getEmployeeOnPosition(position)
            if( currentId!= null) {
                bundle.putInt(EmployeeDetailsActivity.EMPLOYEE_DETAILS_KEY,currentId)
                startActivity<EmployeeDetailsActivity>(bundle)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        adminBusinessClass.getAllEmployees()
    }


}
