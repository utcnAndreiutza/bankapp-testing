package com.example.bankapp.database_layer

import androidx.room.*
import com.example.bankapp.domain_model_layer.Employee


@Dao
interface EmployeeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployee(employee: Employee)

    @Query("UPDATE employee SET name= :name, cnp=:cnp, adress= :adress, phoneNumber=:phoneNumber,email=:email where id=:id")
    fun updateEmployee(id:Int?, name:String, cnp:String, adress:String, phoneNumber:String, email:String)

    @Query("DELETE FROM employee WHERE id ==:id")
    fun deleteEmployeeWithId(id: Int)

    @Query("DELETE FROM employee")
    fun deleteAllEmployees()

    @Query("SELECT * FROM employee WHERE id == :id")
    fun getEmployeeById(id: Int): Employee

    @Query("SELECT * FROM employee")
    fun getAllEmployees(): List<Employee>

    @Query("SELECT * FROM employee where email == :userEmail and password == :userPassword ")
    fun login(userEmail: String, userPassword: String): Employee
}