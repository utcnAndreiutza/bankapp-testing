package com.example.bankapp.domain_model_layer

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull
import java.util.*

@Entity(tableName = "client")
data class Client(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Int? = null,
    val cardId: String,
    var name: String,
    var cnp: String,
    var adress: String,
    var phoneNumber: String,
    var email: String,
    var amount: Double,
    var creationDate: Date
    )