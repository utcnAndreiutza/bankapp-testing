package com.example.bankapp.business_layer.interfaces

interface LoginBusinessInterface {
    fun login(userEmail:String, userPassword:String)
    fun isEmailValid(userEmail:String):Boolean
    fun isPasswordValid(userPassword:String):Boolean
}