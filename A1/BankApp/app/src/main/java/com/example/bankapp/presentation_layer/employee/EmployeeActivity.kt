package com.example.bankapp.presentation_layer.employee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.EmployeeBusinessClass
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.domain_model_layer.DisplayableUser
import com.example.bankapp.presentation_layer.showToast
import com.example.bankapp.presentation_layer.startActivity
import kotlinx.android.synthetic.main.activity_employee.*
import kotlinx.android.synthetic.main.user_details.view.*
import java.util.*

class EmployeeActivity : AppCompatActivity() {

    companion object{
        const val EMPLOYEE_USER_KEY = "EmployeeActivity.user.key"
    }
    private val currentUser : DisplayableUser by lazy{
        intent.getParcelableExtra<DisplayableUser>(EMPLOYEE_USER_KEY)
    }
    private lateinit var employeeBusinessClass :EmployeeBusinessClass


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)
        employeeBusinessClass = EmployeeBusinessClass( AppDatabase.getAppDataBase(applicationContext)?.getClientDao(),  AppDatabase.getAppDataBase(applicationContext)?.getRaportDao())
        employeeBusinessClass.getAllClients()
        currentUser.id?.let { employeeBusinessClass.insertRaport(it,currentUser.name +" has logged in.") }
        welcomeText.text = String.format(getString(R.string.welcome_user),currentUser.name)

        logOutButton.setOnClickListener{
            currentUser.id?.let { employeeBusinessClass.insertRaport(it,currentUser.name +" has logged out.") }
            finish()
        }

        employeeBusinessClass.clientList.observe(this, Observer {clientList->
            setListOfClients(clientList)
        })

        addClient.setOnClickListener{
            addClientDialog()
        }
    }

    private fun addClientDialog() {

        val dialogBuilder = AlertDialog.Builder(this).create()
        val view= LayoutInflater.from(this).inflate(R.layout.user_details,null)
        view.setBackgroundResource(R.drawable.backgroud)
        view.addEmpoyeeButtonDialog.visibility = View.VISIBLE
        dialogBuilder.setView(view)
        val name=view.nameEditText
        val cnp=view.cnpNumberEditText
        val adress=view.adressEditText
        val phoneNumber=view.phoneNumberEditText
        val email=view.emailEditText
        view.addEmpoyeeButtonDialog.setOnClickListener{
            val client = Client(cardId = employeeBusinessClass.generateCardId() ,name=name.text.toString(),cnp=cnp.text.toString(),adress = adress.text.toString(),phoneNumber = phoneNumber.text.toString(),email = email.text.toString(),amount = 0.0,creationDate = Date() )
            if(employeeBusinessClass.isClientDataValid(client)){
                employeeBusinessClass.insertClient(client)
                currentUser.id?.let {  employeeBusinessClass.insertRaport(it,"Insert client "+client.name) }
                dialogBuilder.cancel()
                employeeBusinessClass.getAllClients()
            }else{
                showToast(applicationContext,getString(R.string.invalid_data_to_add_user))
            }
        }
        dialogBuilder.show()
    }

    private fun setListOfClients(clients:List<Client>) {
        val list = clients.map{ it.name}
        val adapter = ArrayAdapter(this, R.layout.client_listview_layout, list)
        clientList.adapter = adapter
        clientList.setOnItemClickListener { _, _, position, _ ->
            val bundle = Bundle()
            val currentClient = employeeBusinessClass.getClientFromList(position)
            if( currentClient!= null) {
                currentClient.id?.let { bundle.putInt(ClientDetailsActivity.CLIENT_ID_KEY, it) }
                bundle.putString(ClientDetailsActivity.CLIENT_NAME_KEY,currentClient.name)
                currentUser.id?.let { bundle.putInt(ClientDetailsActivity.EMPLOYEE_ID_KEY, it) }
                startActivity<ClientDetailsActivity>(bundle)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        employeeBusinessClass.getAllClients()
    }
}
