package com.example.bankapp.presentation_layer.employee

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.bankapp.R
import com.example.bankapp.business_layer.classes.EmployeeBusinessClass
import com.example.bankapp.business_layer.getDateFormat
import com.example.bankapp.database_layer.AppDatabase
import com.example.bankapp.domain_model_layer.Client
import com.example.bankapp.presentation_layer.showToast
import kotlinx.android.synthetic.main.activity_client_details.*
import kotlinx.android.synthetic.main.pay_bill_layout.view.*
import kotlinx.android.synthetic.main.pay_bill_layout.view.waterLayout
import kotlinx.android.synthetic.main.set_amount_layout.view.closeButton
import kotlinx.android.synthetic.main.set_amount_layout.view.doneButton
import kotlinx.android.synthetic.main.set_amount_layout.view.moneyEditText
import kotlinx.android.synthetic.main.set_amount_layout.view.titleLabel
import kotlinx.android.synthetic.main.transfer_amount_layout.view.*
import kotlinx.android.synthetic.main.user_details.*
import java.util.*

class ClientDetailsActivity : AppCompatActivity() {
    private lateinit var employeeBusinessClass : EmployeeBusinessClass
    private val employeeId: Int by lazy{
        intent.getIntExtra(EMPLOYEE_ID_KEY,1)
    }
    private val clientID: Int by lazy {
        intent.getIntExtra(CLIENT_ID_KEY, 1)
    }
    private val clientName: String by lazy {
        intent.getStringExtra(CLIENT_NAME_KEY)
    }

    companion object {
        const val CLIENT_ID_KEY = "Client.id.key"
        const val CLIENT_NAME_KEY = "Client.name.key"
        const val EMPLOYEE_ID_KEY = "Employee.id.key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client_details)
        employeeBusinessClass = EmployeeBusinessClass( AppDatabase.getAppDataBase(applicationContext)?.getClientDao(),  AppDatabase.getAppDataBase(applicationContext)?.getRaportDao())
        deleteClient.setOnClickListener {
            employeeBusinessClass.deleteClientWithId(clientID)
            employeeBusinessClass.insertRaport(employeeId, "Delete client $clientName")
            finish()
        }

        addMoneyButton.setOnClickListener {
            processMoneyDialog(getString(R.string.add_money), true)
        }

        getMoneyButton.setOnClickListener {
            processMoneyDialog(getString(R.string.extract_money), false)
        }

        payBillButton.setOnClickListener {
            payBillDialog()
        }

        transferMoneyButton.setOnClickListener {
            transferMoneyDialog()
        }
        checkDone.setOnClickListener {
            val name = nameEditText.text.toString()
            val cnp = cnpNumberEditText.text.toString()
            val adress = adressEditText.text.toString()
            val phoneNumber = phoneNumberEditText.text.toString()
            val email = emailEditText.text.toString()
            val amount = currentAccount.text.toString().toDouble()
            val currentClient = Client(
                id = clientID,
                cardId = "",
                name = name,
                cnp = cnp,
                adress = adress,
                phoneNumber = phoneNumber,
                email = email,
                amount = amount,
                creationDate = Date()
            )
            if (employeeBusinessClass.isClientDataValid(currentClient)) {
                employeeBusinessClass.updateClient(currentClient)
                employeeBusinessClass.insertRaport(employeeId,"Update client $clientName")
                finish()
            } else {
                showToast(applicationContext,"Update error. Client data invalid.\n 1)Check empty fields\n 2)Make sure that CNP and email are correct.")
            }
        }
    }

    private fun transferMoneyDialog() {
        val dialogBuilder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.transfer_amount_layout, null)
        dialogBuilder.setView(view)
        val moneyInput = view.moneyEditText
        val cardIdInput = view.cardIdEditText
        view.closeButton.setOnClickListener {
            dialogBuilder.cancel()
        }
        view.doneButtonTransfer.setOnClickListener {
            val amountValue = moneyInput.text.toString().toDouble()
            if (employeeBusinessClass.isAmountValid(amountValue)) {

                val currentAmount = currentAccount.text.toString().toDouble()
                if (currentAmount - amountValue >= 0) {
                    val toCardId = cardIdInput.text.toString()
                    employeeBusinessClass.transactionBetweenAccounts(
                        clientID,
                        toCardId,
                        amountValue
                    )
                    dialogBuilder.cancel()
                    refreshCurrentClient()
                    employeeBusinessClass.insertRaport(employeeId,"Transfer of $amountValue$ from ${clientName} to $toCardId")
                } else showToast(applicationContext,"Not enough money to start transfer.")

            } else {
                showToast(applicationContext,"Invalid amount. Be sure that amount > 0")
            }
        }
        dialogBuilder.show()
    }

    private fun processMoneyDialog(title: String, addMoney: Boolean) {
        val dialogBuilder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.set_amount_layout, null)
        dialogBuilder.setView(view)
        view.titleLabel.text = title
        val amount = view.moneyEditText
        view.closeButton.setOnClickListener {
            dialogBuilder.cancel()
        }
        view.doneButton.setOnClickListener {
            val amountValue = amount.text.toString().toDouble()
            if (employeeBusinessClass.isAmountValid(amountValue)) {
                if (addMoney) {
                    employeeBusinessClass.addMoney(clientID, amountValue)
                    dialogBuilder.cancel()
                    refreshCurrentClient()
                    employeeBusinessClass.insertRaport(employeeId,"Add ${amountValue}\$ to "+clientName)
                } else {
                    val currentAmount = currentAccount.text.toString().toDouble()
                    if (currentAmount - amountValue > 0) {
                        employeeBusinessClass.extractMoneyFromAccount(clientID, amountValue)
                        dialogBuilder.cancel()
                        refreshCurrentClient()
                        employeeBusinessClass.insertRaport(employeeId,"Extract ${amountValue}$ from "+clientName)
                    } else showToast(applicationContext, "Not enough money to extract.")
                }
            } else {
                showToast(applicationContext,"Invalid amount. Be sure that amount > 0")
            }
        }
        dialogBuilder.show()
    }

    private fun payBillDialog() {

        val dialogBuilder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.pay_bill_layout, null)
        dialogBuilder.setView(view)
        val waterInput = view.waterEditText
        val gasInput = view.gasEditText
        val electricityInput = view.electricityEditText
        view.closeButton.setOnClickListener {
            dialogBuilder.cancel()
        }
        view.waterLayout.setOnClickListener {
            waterInput.visibility = View.VISIBLE
        }
        view.gasLayout.setOnClickListener {
            gasInput.visibility = View.VISIBLE
        }
        view.electricityLayout.setOnClickListener {
            electricityInput.visibility = View.VISIBLE
        }

        view.doneButton.setOnClickListener {
            if (employeeBusinessClass.areAmountsValid(
                    waterInput.text.toString(),
                    gasInput.text.toString(),
                    electricityInput.text.toString()
                )
            ) {
                val waterAmount = waterInput.text.toString().toDouble()
                val gasAmount = gasInput.text.toString().toDouble()
                val electricityAmount = electricityInput.text.toString().toDouble()
                val currentAmount = currentAccount.text.toString().toDouble()
                val totalSum = waterAmount + gasAmount + electricityAmount
                if (currentAmount - totalSum > 0) {
                    if(waterAmount>0) {
                        employeeBusinessClass.transactionBetweenAccounts(clientID,getString(R.string.water_system_id),waterAmount)
                        employeeBusinessClass.insertRaport(employeeId,"Pay water bill: ${waterAmount}\$ of "+clientName)
                    }
                    if(gasAmount>0) {
                        employeeBusinessClass.transactionBetweenAccounts(clientID,getString(R.string.gas_system_id),gasAmount)
                        employeeBusinessClass.insertRaport(employeeId,"Pay gas bill: ${gasAmount}\$ of "+clientName)
                    }
                    if(electricityAmount>0) {
                        employeeBusinessClass.transactionBetweenAccounts(clientID,getString(R.string.electricity_system_id),electricityAmount)
                        employeeBusinessClass.insertRaport(employeeId,"Pay electricity bill ${electricityAmount}\$ of "+clientName)
                    }
                    dialogBuilder.cancel()
                    refreshCurrentClient()
                } else showToast(applicationContext,"Not enough money to pay.")

            } else {
                showToast(applicationContext,"Invalid amount. \n1)Be sure that amount > 0\n 2)Use point for floating numbers")
            }
        }
        dialogBuilder.show()
    }

    override fun onStart() {
        super.onStart()

        refreshCurrentClient()

        employeeBusinessClass.client.observe(this, Observer { client ->
            nameEditText.setText(client.name)
            cnpNumberEditText.setText(client.cnp)
            adressEditText.setText(client.adress)
            phoneNumberEditText.setText(client.phoneNumber)
            emailEditText.setText(client.email)
            currentAccount.text = client.amount.toString()
            accountId.text = String.format(
                getString(R.string.account_id_is),
                client.id.toString() + client.cardId
            )
            creationDate.text = String.format(
                getString(R.string.account_was_created_at),
                getDateFormat(client.creationDate)
            )
        })
    }

    private fun refreshCurrentClient() {
        employeeBusinessClass.getClientWithId(clientID)
    }

    override fun onStop() {
        super.onStop()
        employeeBusinessClass.client.removeObservers(this)
    }
}
