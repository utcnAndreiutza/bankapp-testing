package com.example.bankapp.business_layer.interfaces
import com.example.bankapp.domain_model_layer.Client


interface EmployeeRequestsInterface{
    fun getAllClients()
    fun getClientWithId(id:Int)
    fun updateClient(client: Client)
    fun insertClient(client: Client)
    fun deleteClientWithId(id:Int)
    fun addMoney(id:Int, amount: Double)
    fun extractMoneyFromAccount(id:Int, amount: Double)
    fun transactionBetweenAccounts(currentId:Int, toAccountNumber: String, amount: Double)
}