package com.example.bankapp.domain_model_layer

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull
import java.util.*

@Entity(tableName = "history")
data class Raport(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    val employeeId: Int,
    val description: String,
    val creationDate: Date
    )